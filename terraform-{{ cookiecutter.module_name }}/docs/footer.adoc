== VPC.dev

This Terraform module is a component of the https://github.com/vpc-dev[VPC.dev] Project.

CAUTION: You should never rely on a repository you do not control for your infrastructure management. _We strongly recommend forking this module repository_ into your own account to avoid unexpected changes to your infrastructure.

Our Terraform modules are highly active repositories. Be sure to pull new changes with care. Any additions you make to your fork can be easily sent back upstream with a quick pull request!

== Support

This Terraform module is maintained and funded by https://vpc.dev[VPC.dev]. VPC.dev specializes in cloud infrastructure &amp; automation.

If you require link:SUPPORT.adoc[Priority Support] for your project, please contact us.

== License

----
   Copyright {% now 'local', '%Y' %}, {{ cookiecutter.org_name }}

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       https://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
----
