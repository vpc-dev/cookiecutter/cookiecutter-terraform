#!/usr/bin/env bash

git init

if command -v pre-commit &> /dev/null; then
    if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" = "true" ]; then
        pre-commit install
        pre-commit autoupdate
    fi
else
    echo "You should install pre-commit and run 'pre-commit install'"
fi

if command -v tflint &> /dev/null; then
    tflint --init
else
    echo "You should install tflint and run 'tflint --init'."
fi
